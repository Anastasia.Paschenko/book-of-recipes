# Recipe book

## Task
Create a single page application for storing recipes

## Functionality
1) Authentication
2) Render all recipes
3) Create a new recipe (only for authorized users)
4) Delete your recipe
5) Update your recipe
6) Likes
7) Search by title, description, ingredients fields
8) Filter by complexity
9) Order by Date (ASC/DESC), Views (DESC), Likes (DESC)

Unathorized user can not: 
1) Update/Delete recipes
2) Like/Dislike recipe

## Setup
### BE
1) create a database
2) create S3 bucket
Note: S3 bucket must be publicly readable by all users
3) run 'npm install'
4) create .env.local file in the be folder. Copy variables names from .env file and initialize them
5) run 'npm run create-tables' to create tables
6) run 'npm run seeds' to create dummy rows in database
7) run 'npm start' or 'npm start:dev' (if you want to have automatically restarted application when file changes)

### FE
1) run 'npm install'
2) create .env.local file in the be folder. Copy variables names from .env file and initialize them
3) run 'npm start'


## Seed users
1) email: 'john@john.com'
   password: 'password'
2) email: 'ann@ann.com'
   password: 'password'