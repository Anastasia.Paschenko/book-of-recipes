import { Button, FormHelperText } from '@mui/material';
import { FC, useState } from 'react';
import axios from '../axios/axios';

interface ImageUploaderProps {
    filePath: string,
    onFilePathChange: (value: string) => void
}

const ImageUploader: FC<ImageUploaderProps> = ({ filePath, onFilePathChange }) => {
  const [error, setError] = useState(false);

  const removeFile = () => {
    onFilePathChange('');
  };

  const uploadFile = (e: any) => {
    const selectedFile = e.target.files[0];
    setError(false);

    if (!selectedFile) return;

    if (!(selectedFile.type.includes('jpeg') || selectedFile.type.includes('jpg') || selectedFile.type.includes('png'))) {
      setError(true);
      return;
    }

    e.preventDefault();
    const formData = new FormData();
    formData.append('image', selectedFile);

    axios.post('/uploadFile', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
      .then((res) => {

        onFilePathChange(res.data.path);
      })
      .catch(() => {
        setError(true);
      });
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      {filePath && <img style={{ maxWidth: '50%', alignSelf: 'center' }} src={filePath} />}
      {filePath && <FormHelperText>File uploaded successfully</FormHelperText>}
      {error && <FormHelperText error={true}>Something went wrong. Try to upload another file</FormHelperText>}
      {filePath &&
                <Button
                  variant="contained"
                  component="label"
                  style={{ marginTop: '10px' }}
                  onClick={removeFile}
                >
                    Remove File
                </Button>
      }
      {!filePath &&
                <Button
                  variant="contained"
                  component="label"
                  style={{ marginTop: '10px' }}
                >
                    Upload File
                  <input
                    accept='.png, .jpg, .jpeg'
                    type="file"
                    hidden
                    onChange={uploadFile}
                  />
                </Button>
      }
    </div>
  );
};

export default ImageUploader;