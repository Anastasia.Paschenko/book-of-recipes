import { Button, TextField } from '@mui/material';
import { FC, useState } from 'react';
import { IngredientList } from './IngredientList';
import { Ingredient } from '../Models/RecipeModel';

interface AddIngredientProps {
    setIngredients: (ingredients: Ingredient[]) => void,
    ingredients: Ingredient[]
}

export const AddIngredient: FC<AddIngredientProps> = ({setIngredients, ingredients}) => {
  const [name, setName] = useState<string>('');
  const [alternatives, setAlternatives] = useState<string>('');

  const onNameChange = (event: any) => {
    setName(event.target.value);
  };

  const onAlternativesChange = (event: any) => {
    setAlternatives(event.target.value);
  };

  const addIngredient = () => {
    if (name) {
      setIngredients([...ingredients, { name, alternatives: alternatives ? alternatives.split('\n') : [] }]);
      setName('');
      setAlternatives('');
    }
  };

  const onRemove = (index: number) => {
    setIngredients(ingredients.filter((ing, i) => i !== index));
  };

  return (
    <>
      <IngredientList ingredients={ingredients} onRemove={onRemove} />
      <div style={{ border: '1px solid lightgray', borderRadius: '5px', padding: '10px' }}>
        <TextField
          label="New ingredient"
          variant="standard"
          fullWidth
          style={{ marginBottom: '15px' }}
          value={name}
          onChange={onNameChange}
        />
        <TextField
          style={{ marginBottom: '7px' }}
          fullWidth
          multiline
          value={alternatives}
          onChange={onAlternativesChange}
          rows={3}
          label="Alternatives"
          variant="outlined"
        />
        <Button fullWidth variant="contained" onClick={addIngredient} disabled={!name}>Add</Button>
      </div>
    </>
  );
};