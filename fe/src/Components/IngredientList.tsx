import { Button, Checkbox } from '@mui/material';
import { FC, useState } from 'react';
import ClearIcon from '@mui/icons-material/Clear';
import styles from './IngredientList.module.scss';
import { Ingredient } from '../Models/RecipeModel';

interface IngredientElementProps {
  ingredient: Ingredient,
  onRemove?: (index: number) => void,
  index: number
}

export const IngredientElement: FC<IngredientElementProps> = ({ ingredient, onRemove, index }) => {
  const [alternativeVisible, setAlternativeVisible] = useState(false);

  const onAlterVisibility = () => {
    setAlternativeVisible(prev => !prev);
  };

  const renderAlternative = (alternative: string, index: number) => {
    return (
      <li key={index}>
        {alternative}
      </li>
    );
  };

  return (
    <div className={styles.ingredient}>
      <div style={{ display: 'flex' }}>
        {!onRemove && <Checkbox />}
        <Button onClick={onAlterVisibility} fullWidth style={{ justifyContent: 'flex-start' }}>
          {ingredient.name}
        </Button>
      </div>
      {
        onRemove &&
        <div className={styles.deleteBtn}>
          <Button onClick={() => onRemove(index)}>
            <ClearIcon />
          </Button>
        </div>
      }
      {
        alternativeVisible && ingredient.alternatives?.length > 0 &&
        <>
          <span style={{ paddingLeft: '15px', textDecoration: 'underline' }}>Alternatives:</span>
          <ul className={styles.alternativesContainer}>
            {ingredient.alternatives.map(renderAlternative)}
          </ul>
        </>
      }
    </div >
  );
};

interface IngredientListProps {
  ingredients: Ingredient[],
  onRemove?: (index: number) => void,
}

export const IngredientList: FC<IngredientListProps> = ({ ingredients, onRemove }) => {
  return (
    <>
      <div className={styles.container}>
        {ingredients.map((ingredient, index) => {
          return <IngredientElement key={index} ingredient={ingredient} index={index} onRemove={onRemove} />;
        })}
      </div>
    </>
  );
};