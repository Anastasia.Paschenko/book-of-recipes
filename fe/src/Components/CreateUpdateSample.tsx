import { Button, Paper, TextField, FormHelperText, Dialog, DialogActions, DialogTitle, Select, MenuItem, InputLabel } from '@mui/material';
import { FC, useEffect, useState } from 'react';
import styles from './CreateUpdateSample.module.scss';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertToRaw, EditorState } from 'draft-js';
import draftToHtmlPuri from 'draftjs-to-html';
import { useSelector } from 'react-redux';
import { Ingredient, Complexity, CreateRecipeModel, RecipeModel } from '../Models/RecipeModel';
import { AddIngredient } from './AddIngredient';
import { selectIsAuth, Store } from '../redux/store';
import ImageUploader from './ImageUploader';
import { stateFromHTML } from 'draft-js-import-html';

interface CreateUpdateSampleProps {
    onComplete: (model: CreateRecipeModel) => Promise<number>
    defaultValue?: RecipeModel
}

export const CreateUpdateSample: FC<CreateUpdateSampleProps> = ({ onComplete, defaultValue }) => {
  const userId = useSelector((state: Store) => state.user.result?.id);

  const [method, setMethod] = useState(defaultValue
    ? EditorState.createWithContent(stateFromHTML(defaultValue.method))
    : EditorState.createEmpty());
  const [methodError, setMethodError] = useState(false);

  const [ingredients, setIngredients] = useState<Ingredient[]>(defaultValue?.ingredients || []);
  const [ingredientsError, setIngredientsError] = useState(false);

  const [complexity, setComplexity] = useState<Complexity>(defaultValue?.complexity || Complexity.Easy);
  const [complexityError, setComplexityError] = useState(false);

  const [title, setTitle] = useState<string>(defaultValue?.title || '');
  const [titleError, setTitleError] = useState(false);

  const [description, setDescription] = useState<string>(defaultValue?.description || '');

  const [image, setImage] = useState<string>(defaultValue?.image || '');

  const [tags, setTags] = useState<string>(defaultValue?.tags.join('\n') || '');

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const isAuth = useSelector(selectIsAuth);

  const onEditorStateChange = (editorState: EditorState) => {
    setMethod(editorState);
    if (editorState.getCurrentContent().hasText()) {
      setMethodError(false);
    }
  };

  const isEmptyFields = () => {
    let error = false;
    if (!method.getCurrentContent().hasText()) {
      setMethodError(true);
      error = true;
    }
    if (!complexity) {
      setComplexityError(true);
      error = true;
    }
    if (!title) {
      setTitleError(true);
      error = true;
    }
    if (ingredients.length === 0) {
      setIngredientsError(true);
      error = true;
    }

    return error;
  };

  const onSubmit = async () => {
    try {
      if (isEmptyFields()) return;

      setLoading(true);
      const createdModel: CreateRecipeModel = {
        method: draftToHtmlPuri(convertToRaw(method.getCurrentContent())),
        description: description !== '' ? description : undefined,
        title,
        ingredients: JSON.stringify(ingredients),
        tags: tags.split('\n').filter(t => t),
        complexity: complexity!,
        image: image !== '' ? image : undefined,
      };
      const id = await onComplete(createdModel);

      if (id) {
        navigate(`/recipe/${id}`);
      } else {
        setError(true);
      }
    } catch {
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  const onComplexityChange = (event: any) => {
    setComplexity(event.target.value);
    setComplexityError(false);
  };

  const onTitleChange = (event: any) => {
    setTitle(event.target.value);
    setTitleError(false);
  };

  const onDescriptionChange = (event: any) => {
    setDescription(event.target.value);
  };

  const onTagsChange = (event: any) => {
    setTags(event.target.value);
  };

  const onImageChange = (value: string) => {
    setImage(value);
  };

  useEffect(() => {
    setIngredientsError(false);
  }, [ingredients]);

  if ((!isAuth && !window.localStorage.getItem('token')) || defaultValue?.author.id !== userId) {
    return <Navigate to='/' />;
  }

  return (
    <>
      <Dialog open={error}>
        <DialogTitle >Something went wrong</DialogTitle>
        <DialogActions>
          <Button onClick={() => setError(false)} variant="contained">Ok</Button>
        </DialogActions>
      </Dialog>
      <Paper elevation={4} className={styles.container}>
        <div className={styles.title}>Add new recipe</div>
        <TextField
          style={{ marginBottom: '20px' }}
          fullWidth
          label="Title"
          variant="standard"
          error={titleError}
          value={title}
          helperText={titleError ? 'Title is required' : ''}
          onChange={onTitleChange}
          required
        />
        <TextField
          style={{ marginBottom: '20px' }}
          fullWidth
          multiline
          rows={5}
          label="Description"
          value={description}
          onChange={onDescriptionChange}
          variant="outlined"
        />
        <ImageUploader onFilePathChange={onImageChange} filePath={image} />
        <InputLabel style={{ marginTop: '20px' }} required error={complexityError}>Complexity</InputLabel>
        <Select
          value={complexity}
          fullWidth
          error={complexityError}
          onChange={onComplexityChange}>
          <MenuItem value={Complexity.Easy}>{Complexity.Easy}</MenuItem>
          <MenuItem value={Complexity.Medium}>{Complexity.Medium}</MenuItem>
          <MenuItem value={Complexity.Hard}>{Complexity.Hard}</MenuItem>
        </Select>
        {complexityError && <FormHelperText error={true}>Complexity is required field</FormHelperText>}
        <InputLabel style={{ marginTop: '20px' }} required error={ingredientsError}>Ingredients</InputLabel>
        <div style={{ border: ingredientsError ? '1px solid #d32f2f' : '1px solid lightgray', borderRadius: '4px' }}>
          <AddIngredient setIngredients={setIngredients} ingredients={ingredients} />
        </div>
        {ingredientsError && <FormHelperText error={true}>Ingredients is required field</FormHelperText>}
        <InputLabel required error={methodError} style={{ marginTop: '20px' }}>Method</InputLabel>
        <div style={{ marginBottom: '20px' }}>
          <Editor
            editorStyle={{
              border: methodError ? '1px solid #d32f2f' : '1px solid lightgray', borderRadius: '4px', paddingLeft: '10px', height: '200px'
            }}
            editorState={method}
            onEditorStateChange={onEditorStateChange}
            toolbar={{
              options: ['inline', 'list'],
              inline: {
                options: ['bold', 'italic', 'underline', 'strikethrough'],
              },
              list: {
                options: ['unordered', 'ordered'],
              },
            }}
          />
          {methodError && <FormHelperText error={true}>Method is required field</FormHelperText>}
        </div>
        <TextField
          style={{ marginBottom: '20px' }}
          fullWidth
          multiline
          rows={1}
          label="Tags"
          variant="outlined"
          value={tags}
          onChange={onTagsChange}
        />
        <div className={styles.btnBlock}>
          <Button
            style={{ marginRight: '30px' }}
            size='large'
            variant="contained"
            disabled={loading}
            onClick={onSubmit}
          >
            {defaultValue ? 'Update' : 'Create'}
          </Button>
          <Link to='/' style={{ textDecoration: 'none' }}>
            <Button size='large' variant="outlined" disabled={loading}>Cancel</Button>
          </Link>
        </div>
      </Paper>
    </>
  );
};