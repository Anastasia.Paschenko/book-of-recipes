import { FC, useState } from 'react';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import { MenuBook } from '@mui/icons-material';
import styles from './Header.module.scss';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { selectIsAuth } from '../redux/store';
import { logout } from '../redux/slices/auth';
import { Dialog, DialogTitle, DialogActions } from '@mui/material';

const SignOutBtn: FC = () => {
  const dispatch = useDispatch<any>();
  const [confirmWindowOpen, setConfirmWindowOpen] = useState(false);

  const onLogoutClick = () => {
    setConfirmWindowOpen(true);
  };

  const logoutAction = () => {
    dispatch(logout());
    window.localStorage.removeItem('token');
  };

  return (
    <>
      <Dialog open={confirmWindowOpen}>
        <DialogTitle>Are you sure you want to logout?</DialogTitle>
        <DialogActions>
          <Button onClick={() => setConfirmWindowOpen(false)} variant="contained" autoFocus>Cancel</Button>
          <Button onClick={logoutAction} variant="outlined">Ok</Button>
        </DialogActions>
      </Dialog>
      <Button onClick={onLogoutClick} variant="outlined"> Sign out</Button>
    </>
  );
};

const SignInBtnGroup: FC = () => {
  return (
    <div className={styles.buttonGroup}>
      <Link to={'/signin'} className={styles.logoGroup}>
        <Button variant="contained">Sign in</Button>
      </Link>
      <Link to={'/signup'} className={styles.logoGroup}>
        <Button variant="outlined">Sign up</Button>
      </Link>
    </div>
  );
};

export const Header: FC = () => {
  const isAuth = useSelector(selectIsAuth);

  return (
    <Container maxWidth="lg">
      <div className={styles.container}>
        <Link to={'/'} className={styles.logoGroup}>
          <MenuBook style={{ color: '#292929' }} fontSize="large" />
          <p>Recipe book</p>
        </Link>
        {isAuth ? <SignOutBtn /> : <SignInBtnGroup />}
      </div>
    </Container>
  );
};

