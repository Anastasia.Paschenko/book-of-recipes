import { FC } from 'react';
import styles from './RecipeInfoBlock.module.scss';
import { UserInfo } from './UserInfo';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import { RecipeModel } from '../Models/RecipeModel';
import moment from 'moment';
import { Button } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { selectIsAuth } from '../redux/store';
import { fetchUnlike, fetchLike } from '../redux/fetchers/recipeFetchers';

const Views: FC<{ viewsCount: number }> = ({ viewsCount }) => {
  return (
    <div className={styles.iconBlock}>
      <VisibilityOutlinedIcon />
      <span>{viewsCount}</span>
    </div>
  );
};

const Likes: FC<{ recipeId: number, likeCount: number, isLiked: boolean }> = ({ likeCount, isLiked, recipeId }) => {
  const dispatch = useDispatch<any>();
  const isAuth = useSelector(selectIsAuth);

  const onClick = () => {
    isLiked
      ? dispatch(fetchUnlike(recipeId))
      : dispatch(fetchLike(recipeId));
  };

  const likeContent =
    <>
      {isLiked ? <ThumbUpIcon color="primary" /> : <ThumbUpOffAltIcon />}
      <span>{likeCount}</span>
    </>;

  if (!isAuth) return (
    <div className={styles.iconBlock}>
      {likeContent}
    </div>
  );

  return (
    <div className={styles.iconBlock}>
      <Button onClick={onClick}>
        {likeContent}
      </Button>
    </div>
  );
};


export const RecipeInfoBlock: FC<{ recipe: RecipeModel }> = ({ recipe }) => {
  return (
    <div className={styles.container}>
      <UserInfo user={recipe.author} />
      <span className={styles.text}>Complexity: {recipe.complexity}</span>
      <span className={styles.text}>{moment(recipe.createdAt).format('DD MMM YYYY')}</span>
      <div style={{ display: 'flex', justifyContent: 'space-between', width: '90px' }}>
        <Likes recipeId={recipe.id} likeCount={recipe.likes} isLiked={recipe.isLiked} />
        <Views viewsCount={recipe.views} />
      </div>
    </div>
  );
};