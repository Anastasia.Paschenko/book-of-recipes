import { FC } from 'react';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import styles from './UserInfo.module.scss';
import { UserModel } from '../Models/UserModel';

export const UserInfo: FC<{user: UserModel}> = ({user}) => {
  return (
    <div className={styles.container}>
      <AccountBoxIcon fontSize='large' />
      <span>{user.login}</span>
    </div>
  );
};