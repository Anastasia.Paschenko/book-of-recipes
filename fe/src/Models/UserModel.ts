export type UserModel = {
    id: number,
    login: string,
    email: string,
    role: string,
}
