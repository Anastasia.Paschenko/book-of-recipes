import { UserModel } from './UserModel';

export enum Complexity {
    Easy='EASY',
    Medium='MEDIUM',
    Hard='HARD'
}

export type Ingredient = {
    name: string,
    alternatives: string[]
}

export type RecipeModel = {
    id: number,
    method: string,
    description?: string,
    title: string,
    complexity: Complexity,
    ingredients: Ingredient[],
    tags: string[],
    views: number,
    createdAt: string,
    author: UserModel,
    likes: number,
    isLiked: boolean,
    image?: string,
}

export type CreateRecipeModel = {
    method: string,
    description?: string,
    complexity: Complexity,
    title: string,
    ingredients: string,
    tags: string[],
    image?: string,
}
