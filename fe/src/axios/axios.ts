import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_API,
});

instance.interceptors.request.use(
  (config) => {
    if (config?.headers && window.localStorage.getItem('token')) {
      config.headers.Authorization = `Bearer ${window.localStorage.getItem('token')}`;
    }

    return config;
  },
);

export default instance;
