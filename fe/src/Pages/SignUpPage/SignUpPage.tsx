import { Button, Paper, TextField } from '@mui/material';
import { FC, useState } from 'react';
import styles from './SignUpPage.module.scss';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { selectIsAuth } from '../../redux/store';
import { Navigate } from 'react-router-dom';
import { fetchRegister } from '../../redux/fetchers/authFetchers';

export const SignUpPage: FC<any> = () => {
  const { register, handleSubmit, formState: { errors } } = useForm({
    defaultValues: {
      email: '',
      login: '',
      password: ''
    }
  });
  const dispatch = useDispatch<any>();
  const isAuth = useSelector(selectIsAuth);
  const [isError, setIsError] = useState(false);

  const onSubmit = async (values: any) => {
    const data = await dispatch(fetchRegister(values));
    if (data.payload?.token) {
      window.localStorage.setItem('token', data.payload.token);
    }
    if (data.error) {
      setIsError(true);
    }
  };
    
  if (isAuth) {
    return <Navigate to='/' />;
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Paper elevation={4} className={styles.container}>
        <AccountBoxIcon sx={{ fontSize: 100 }} style={{ marginBottom: '20px' }} />
        {isError && <div className={styles.error}>Failed to register</div>}
        <TextField
          style={{ marginBottom: '20px' }}
          label="Login"
          fullWidth
          error={!!errors.login?.message}
          helperText={errors.login?.message}
          {...register('login', { required: 'Login is required' })}
        />
        <TextField
          style={{ marginBottom: '20px' }}
          label="Email"
          fullWidth
          type='email'
          error={!!errors.email?.message}
          helperText={errors.email?.message}
          {...register('email', { required: 'Email is required' })}
        />
        <TextField
          style={{ marginBottom: '40px' }}
          label="Password"
          fullWidth
          type='password'
          helperText={errors.password?.message}
          error={!!errors.password?.message}
          {...register('password', { required: 'Password is required' })}
        />
        <Button type="submit" size="large" variant="contained">
                    Sign up
        </Button>
      </Paper>
    </form>
  );
};