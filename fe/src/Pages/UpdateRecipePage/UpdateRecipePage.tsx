import { CircularProgress } from '@mui/material';
import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import axios from '../../axios/axios';
import { CreateUpdateSample } from '../../Components/CreateUpdateSample';
import { CreateRecipeModel } from '../../Models/RecipeModel';
import { fetchRecipe } from '../../redux/fetchers/recipeFetchers';
import { Store } from '../../redux/store';
import styles from './UpdateRecipePage.module.scss';

export const UpdateRecipePage: FC = () => {
  const { id } = useParams();
  const dispatch = useDispatch<any>();
  const recipeStore = useSelector((state: Store) => state.recipe);

  useEffect(() => {
    dispatch(fetchRecipe(Number(id)));

    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }, []);

  const onSubmit = async (updatedModel: CreateRecipeModel) => {
    try {
      const { data } = await axios.patch(`/recipes/${id}`, updatedModel);
      return data?.id;
    } catch {
      return 0;
    }
  };
  
  if (recipeStore.loading) return <div className={styles.container}><CircularProgress /></div>;

  return <CreateUpdateSample onComplete={onSubmit} defaultValue={recipeStore.result} />;
};