import { FC } from 'react';
import styles from './Recipe.module.scss';
import { RecipeInfoBlock } from '../../../Components/RecipeInfoBlock';
import { RecipeModel } from '../../../Models/RecipeModel';
import { IconButton, Paper } from '@mui/material';
import { DeleteBtn } from '../../HomePage/Components/DeleteBtn';
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate } from 'react-router-dom';
import { Store } from '../../../redux/store';
import { useSelector } from 'react-redux';

interface RecipeProps {
    recipe: RecipeModel
}

export const Recipe: FC<RecipeProps> = ({ recipe }) => {
  const userId = useSelector((state: Store) => state.user.result?.id);

  const navigate = useNavigate();

  const onDelete = () => {
    navigate('/');
  };

  const goToEdit = () => {
    navigate(`/recipe/update/${recipe.id}`);
  };

  return (
    <Paper elevation={4} className={styles.container}>
      {recipe.author.id === userId && <div className={styles.btnContainer}>
        <IconButton onClick={goToEdit}>
          <EditIcon />
        </IconButton>
        <DeleteBtn recipe={recipe} onDelete={onDelete} />
      </div>}
      <div className={styles.title}>{recipe.title}</div>
      <div className={styles.body}>
        <RecipeInfoBlock recipe={recipe} />
        {recipe.image && <img style={{ maxWidth: '50%', alignSelf: 'center' }} src={recipe.image} />}
        <div className={styles.description}>{recipe.description}</div>
        <div className={styles.methodTitle}>Method</div>
        <div dangerouslySetInnerHTML={{ __html: recipe.method }}></div>
        <div className={styles.tags}>
          {recipe.tags.map((tag, index) => <span key={index}>{tag}</span>)}
        </div>
      </div>
    </Paper>
  );
};