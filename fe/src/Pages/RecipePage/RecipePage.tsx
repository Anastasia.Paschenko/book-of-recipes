import { FC, useEffect } from 'react';
import { Recipe } from './Components/Recipe';
import styles from './RecipePage.module.scss';
import Paper from '@mui/material/Paper';
import { useParams } from 'react-router-dom';
import { CircularProgress } from '@mui/material';
import { IngredientList } from '../../Components/IngredientList';
import { useDispatch, useSelector } from 'react-redux';
import { fetchRecipe } from '../../redux/fetchers/recipeFetchers';
import { Store } from '../../redux/store';


export const RecipePage: FC<any> = () => {
  const { id } = useParams();
  const dispatch = useDispatch<any>();
  const recipeStore = useSelector((state: Store) => state.recipe);

  useEffect(() => {
    dispatch(fetchRecipe(Number(id)));

    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }, []);

  if (recipeStore.loading) return <div className={styles.container}><CircularProgress /></div>;

  if (recipeStore.error || !recipeStore.result) return <div className={styles.container}>Something went wrong</div>;

  return (
    <div className={styles.container}>
      <div className={styles.recipeContainer}>
        <Recipe recipe={recipeStore.result}></Recipe>
      </div>
      <Paper elevation={4} className={styles.ingredientsContainer}>
        <div className={styles.ingredientsLabel}>Ingredients:</div>
        <IngredientList ingredients={recipeStore.result.ingredients} />
      </Paper>
    </div>
  );
};
