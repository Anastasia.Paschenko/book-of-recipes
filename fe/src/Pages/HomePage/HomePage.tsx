import { Button, CircularProgress, IconButton, Pagination, TextField } from '@mui/material';
import { FC, useEffect, useState } from 'react';
import { Recipe } from './Components/Recipe';
import styles from './HomePage.module.scss';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { selectIsAuth, Store } from '../../redux/store';
import { defaultOrder, OrderBtn, OrderProps } from './Components/OrderBtn';
import AddIcon from '@mui/icons-material/Add';
import { fetchRecipes } from '../../redux/fetchers/recipeFetchers';
import SearchIcon from '@mui/icons-material/Search';
import { FilterBtn, FilterProps } from './Components/FilterBtn';
import ClearIcon from '@mui/icons-material/Clear';

export const HomePage: FC<any> = () => {
  const dispatch = useDispatch<any>();
  const recipeResult = useSelector((state: Store) => state.recipes);
  const isAuth = useSelector(selectIsAuth);
  const limit = 10;
  const [currentPage, setCurrentPage] = useState(1);
  const [orderBy, setOrderBy] = useState<OrderProps>(defaultOrder);
  const [filters, setFilters] = useState<FilterProps>();
  const [searchValue, setSearchValue] = useState<string>('');

  const isLoading = recipeResult.loading;

  const updateRecipes = (page = currentPage, search = searchValue) => {
    const fullFilters = {
      searchString: search ? search : undefined,
      complexity: filters?.complexity
    };

    dispatch(fetchRecipes({ page, limit, orderBy, filters: fullFilters }));
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  };

  useEffect(() => {
    updateRecipes();
  }, [currentPage, orderBy, filters]);

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setCurrentPage(value);
  };

  const onDelete = () => {
    if (recipeResult.result && recipeResult.result.items.length <= 1) {
      updateRecipes(currentPage - 1);
      setCurrentPage((p) => p - 1);
    } else {
      updateRecipes();
    }
  };

  const onSearchChange = (event: any) => {
    setSearchValue(event.target.value);
  };

  const onSearchClick = () => {
    updateRecipes();
  };

  const onRemoveSearchClick = () => {
    setSearchValue('');
    updateRecipes(currentPage, '');
  };

  if (isLoading) return <div className={styles.container}><CircularProgress /></div>;

  if (recipeResult.error) return <div className={styles.container}>Something went wrong</div>;

  return (
    <>
      {recipeResult.result &&
                <div className={styles.container}>
                  <div className={styles.btnContainer}>
                    <div style={{ display: 'flex', width: '75%' }}>
                      <TextField
                        fullWidth
                        style={{ background: 'white' }}
                        onChange={onSearchChange}
                        value={searchValue}
                        InputProps={{
                          endAdornment: (
                            <IconButton
                              sx={{ visibility: searchValue ? 'visible' : 'hidden' }}
                              onClick={onRemoveSearchClick}
                            >
                              <ClearIcon />
                            </IconButton>
                          ),
                        }}
                      />
                      <Button style={{ marginLeft: '5px' }} variant="contained" onClick={onSearchClick}>
                        <SearchIcon />
                      </Button>
                    </div>
                    <FilterBtn chosenFilter={filters} onChangeFilter={setFilters} />
                    <OrderBtn onOrder={setOrderBy} chosenOrder={orderBy} />
                    {isAuth && <Link to='/recipe/create' style={{ textDecoration: 'none' }} >
                      <Button variant="contained" style={{ height: '100%' }}>
                        <AddIcon />
                      </Button>
                    </Link>}
                  </div>
                  {recipeResult.result?.items.length === 0
                    ? <div style={{ marginTop: '200px', fontSize: '30px' }}>No result</div>
                    : <>
                      <div style={{ width: '100%' }}>
                        {recipeResult.result.items.map((recipe) => {
                          return <Recipe key={recipe.id} recipe={recipe} onDelete={onDelete} />;
                        })}
                      </div>
                      <Pagination count={Math.ceil(recipeResult.result.totalCount / limit)} page={currentPage} onChange={handleChange} />
                    </>
                  }
                </div>
      }
    </>
  );
};