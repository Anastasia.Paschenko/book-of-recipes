import SwapVertIcon from '@mui/icons-material/SwapVert';
import { Button, InputLabel, MenuItem, Popover, Select, SelectChangeEvent } from '@mui/material';
import { FC, useState } from 'react';

export type OrderProps = {
    field: string,
    type: 'ASC' | 'DESC'
}

export const defaultOrder: OrderProps = {
  field: 'createdAt',
  type: 'DESC'
};

const OrderOptions: { title: string, value: OrderProps }[] = [
  {
    title: 'Newest First',
    value: {
      field: 'createdAt',
      type: 'DESC'
    }
  },
  {
    title: 'Oldest First',
    value: {
      field: 'createdAt',
      type: 'ASC'
    }
  },
  {
    title: 'Most Popular',
    value: {
      field: 'views',
      type: 'DESC'
    }
  },
  {
    title: 'Highest rating',
    value: {
      field: 'likes',
      type: 'DESC'
    }
  }
];

interface OrderBtnProps {
    chosenOrder: OrderProps,
    onOrder: (value: OrderProps) => void
}

export const OrderBtn: FC<OrderBtnProps> = ({ onOrder, chosenOrder }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [order, setOrder] = useState<string>(JSON.stringify(chosenOrder));
  const open = Boolean(anchorEl);

  const onOrderClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onOrderClose = () => {
    setAnchorEl(null);
  };

  const onChange = (event: SelectChangeEvent<string>) => {
    setOrder(event.target.value);
  };

  const onApply = () => {
    onOrder(JSON.parse(order));
  };

  return (
    <>
      <Button variant="contained" onClick={onOrderClick} >
        <SwapVertIcon />
      </Button >
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={onOrderClose}
        style={{ marginTop: '10px' }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <div style={{ padding: '15px', display: 'flex', flexDirection: 'column' }}>
          <InputLabel>Order by</InputLabel>
          <Select
            style={{ width: '150px' }}
            value={order}
            onChange={onChange}
          >
            {OrderOptions.map(option => {
              return <MenuItem key={option.title} value={JSON.stringify(option.value)}>{option.title}</MenuItem>;
            })}
          </Select>
          <Button variant="outlined" onClick={onApply} style={{ marginTop: '10px' }}>Apply</Button>
        </div>
      </Popover>

    </>
  );
};
