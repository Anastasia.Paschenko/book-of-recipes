import { Dialog, DialogTitle, DialogActions, Button, IconButton } from '@mui/material';
import { FC, useState } from 'react';
import { RecipeModel } from '../../../Models/RecipeModel';
import DeleteIcon from '@mui/icons-material/Clear';
import axios from '../../../axios/axios';

interface DeleteBtnProps {
    recipe: RecipeModel,
    onDelete: () => void
}
export const DeleteBtn: FC<DeleteBtnProps> = ({ recipe, onDelete }) => {
  const [confirmWindowOpen, setConfirmWindowOpen] = useState(false);

  const onRemoveClick = () => {
    setConfirmWindowOpen(true);
  };

  const removeAction = async () => {
    await axios.delete(`/recipes/${recipe.id}`);
    onDelete();
  };

  return (
    <>
      <Dialog open={confirmWindowOpen}>
        <DialogTitle>Are you want to delete {recipe.title}?</DialogTitle>
        <DialogActions>
          <Button onClick={() => setConfirmWindowOpen(false)} variant="contained" autoFocus>Cancel</Button>
          <Button onClick={removeAction} variant="outlined">Ok</Button>
        </DialogActions>
      </Dialog>
      <IconButton onClick={onRemoveClick}>
        <DeleteIcon />
      </IconButton>
    </>
  );
};