import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { Button, IconButton, InputLabel, MenuItem, Popover, Select } from '@mui/material';
import { FC, useState } from 'react';
import { Complexity } from '../../../Models/RecipeModel';
import ClearIcon from '@mui/icons-material/Clear';

export type FilterProps = {
    complexity?: Complexity,
}

interface FilterBtnProps {
    chosenFilter?: FilterProps,
    onChangeFilter: (value?: FilterProps) => void
}

export const FilterBtn: FC<FilterBtnProps> = ({ onChangeFilter, chosenFilter }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [complexity, setComplexity] = useState<string>(chosenFilter?.complexity?.toString() || '');
  const open = Boolean(anchorEl);

  const onFilterClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onFilterClose = () => {
    setAnchorEl(null);
  };

  const onComplexityChange = (event: any) => {
    setComplexity(event.target.value);
  };

  const onApply = () => {
    onChangeFilter({ complexity: complexity ? complexity as Complexity : undefined });
  };

  return (
    <>
      <Button variant="contained" onClick={onFilterClick}>
        <FilterAltIcon />
      </Button>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={onFilterClose}
        style={{ marginTop: '10px' }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <div style={{ padding: '15px', display: 'flex', flexDirection: 'column' }}>
          <InputLabel>Complexity</InputLabel>
          <Select
            value={complexity}
            style={{ width: '200px' }}
            onChange={onComplexityChange}
            fullWidth
            displayEmpty
            sx={{ '& .MuiSelect-iconOutlined': { display: complexity ? 'none' : '' } }}
            renderValue={(value) => value ? value : <em>Not Selected</em>}
            endAdornment={<IconButton sx={{ visibility: complexity ? 'visible' : 'hidden' }} onClick={() => setComplexity('')}><ClearIcon /></IconButton>}
          >
            <MenuItem value={'EASY'}>{Complexity.Easy}</MenuItem>
            <MenuItem value={'MEDIUM'}>{Complexity.Medium}</MenuItem>
            <MenuItem value={'HARD'}>{Complexity.Hard}</MenuItem>
          </Select>
          <Button variant="outlined" onClick={onApply} style={{ marginTop: '10px' }}>Apply</Button>
        </div>
      </Popover>

    </>
  );
};

