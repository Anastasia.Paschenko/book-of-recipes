import { FC } from 'react';
import styles from './Recipe.module.scss';
import { RecipeInfoBlock } from '../../../Components/RecipeInfoBlock';
import { Ingredient, RecipeModel } from '../../../Models/RecipeModel';
import { Link } from 'react-router-dom';
import { IconButton, Paper } from '@mui/material';
import { DeleteBtn } from './DeleteBtn';
import EditIcon from '@mui/icons-material/Edit';
import { useSelector } from 'react-redux';
import { Store } from '../../../redux/store';

interface RecipeProps {
    recipe: RecipeModel
    onDelete: () => void
}

export const Recipe: FC<RecipeProps> = ({ recipe, onDelete }) => {
  const userId = useSelector((state: Store) => state.user.result?.id);
    
  return (
    <Paper elevation={4} className={styles.container}>
      {recipe.author.id === userId && <div className={styles.btnContainer}>
        <Link to={`/recipe/update/${recipe.id}`}>
          <IconButton>
            <EditIcon />
          </IconButton>
        </Link>
        <DeleteBtn recipe={recipe} onDelete={onDelete} />
      </div>}
      <Link to={`/recipe/${recipe.id}`} className={styles.title}>
        {recipe.title}
      </Link>
      <div className={styles.body}>
        {recipe.image && <img style={{ maxWidth: '50%', alignSelf: 'center' }} src={recipe.image} />}
        <div className={styles.description}>{recipe.description}</div>
        <div className={styles.ingredientsContainer}>
          <span>Ingredients: </span>
          {recipe.ingredients.map((ing: Ingredient) => ing.name).join(', ')}
        </div>
        <div className={styles.tags}>
          {recipe.tags.map((tag, index) => <span key={index}>{tag}</span>)}
        </div>
        <RecipeInfoBlock recipe={recipe} />
      </div>
    </Paper>
  );
};
