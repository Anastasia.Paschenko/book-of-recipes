import { FC } from 'react';
import axios from '../../axios/axios';
import { CreateUpdateSample } from '../../Components/CreateUpdateSample';
import { CreateRecipeModel } from '../../Models/RecipeModel';

export const CreateRecipePage: FC = () => {
  const onSubmit = async (createdModel: CreateRecipeModel) => {
    try {
      const { data } = await axios.post('/recipes', createdModel);
      return data?.id;
    } catch {
      return 0;
    }
  };

  return <CreateUpdateSample onComplete={onSubmit} />;
};
