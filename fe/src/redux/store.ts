import { configureStore } from '@reduxjs/toolkit';
import { RecipesStore, RecipeStore, UserStore } from './dataStore';
import { recipeReducer, recipesReducer } from './slices/recipe';
import { authReducer } from './slices/auth';

export interface Store {
    recipes: RecipesStore,
    recipe: RecipeStore,
    user: UserStore
}

const store = configureStore({
  reducer: {
    recipes: recipesReducer,
    recipe: recipeReducer,
    user: authReducer,
  },
});

export default store;

export const selectIsAuth = (state: Store) => state.user.result?.id;
