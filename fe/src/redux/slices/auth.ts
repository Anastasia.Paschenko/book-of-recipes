import { createSlice } from '@reduxjs/toolkit';
import { UserModel } from '../../Models/UserModel';
import { DataStore, reducerSample } from '../dataStore';
import { fetchLogin, fetchMe, fetchRegister } from '../fetchers/authFetchers';

const initialState: DataStore<UserModel> = {
  result: undefined,
  loading: true,
  error: undefined,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: (state) => {
      state.result = undefined;
    },
  },
  extraReducers: (builder) => {
    reducerSample(builder, fetchLogin);
    reducerSample(builder, fetchMe);
    reducerSample(builder, fetchRegister);
  },
});

export const authReducer = authSlice.reducer;

export const { logout } = authSlice.actions;
