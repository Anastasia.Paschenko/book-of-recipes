import { createSlice } from '@reduxjs/toolkit';
import { RecipesStore, RecipeStore } from '../dataStore';
import {
  fetchRecipes, fetchLike, fetchUnlike, fetchRecipe,
} from '../fetchers/recipeFetchers';

const initialRecipesState: RecipesStore = {
  result: undefined,
  loading: true,
  error: undefined,
};

const recipesSlice = createSlice({
  name: 'recipes',
  initialState: initialRecipesState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchRecipes.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchRecipes.fulfilled, (state, { payload }) => {
      const items = payload.items.map((recipe: any) => {
        const { ingredients, ...fields } = recipe;
        return { ...fields, ingredients: JSON.parse(ingredients) };
      });

      state.result = { items, totalCount: payload.totalCount };
      state.loading = false;
    });
    builder.addCase(fetchRecipes.rejected, (state, { error }) => {
      state.result = undefined;
      state.loading = false;
      state.error = error;
    });

    builder.addCase(fetchLike.fulfilled, (state, { meta }) => {
      if (state.result) {
        state.result.items = state.result.items.map((el) => (el.id !== meta.arg ? el : { ...el, isLiked: true, likes: el.likes! + 1 }));
      }
    });

    builder.addCase(fetchUnlike.fulfilled, (state, { payload, meta }) => {
      if (state.result && payload.amountOfDeletedItems) {
        state.result.items = state.result.items.map((el) => (el.id !== meta.arg ? el : { ...el, isLiked: false, likes: el.likes! - 1 }));
      }
    });
  },
});

const initialRecipeState: RecipeStore = {
  result: undefined,
  loading: true,
  error: undefined,
};

const recipeSlice = createSlice({
  name: 'recipe',
  initialState: initialRecipeState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchRecipe.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchRecipe.fulfilled, (state, { payload }) => {
      const { ingredients, ...fields } = payload;
      state.result = { ...fields, ingredients: JSON.parse(ingredients) };
      state.loading = false;
    });
    builder.addCase(fetchRecipe.rejected, (state, { error }) => {
      state.result = undefined;
      state.loading = false;
      state.error = error;
    });

    builder.addCase(fetchLike.fulfilled, (state, { meta }) => {
      if (state.result?.id === meta.arg) {
        state.result = { ...state.result, isLiked: true, likes: state.result.likes! + 1 };
      }
    });

    builder.addCase(fetchUnlike.fulfilled, (state, { payload, meta }) => {
      if (state.result?.id === meta.arg && payload) {
        state.result = { ...state.result, isLiked: false, likes: state.result.likes! - 1 };
      }
    });
  },
});

export const recipeReducer = recipeSlice.reducer;

export const recipesReducer = recipesSlice.reducer;
