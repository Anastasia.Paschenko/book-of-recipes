import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from '../../axios/axios';
import { Complexity } from '../../Models/RecipeModel';
import { OrderProps } from '../../Pages/HomePage/Components/OrderBtn';

type SearchParams = {
    page: number,
    limit: number,
    orderBy?: OrderProps,
    filters?: { searchString?: string, complexity?: Complexity }
}

export const fetchRecipes = createAsyncThunk('recipes/fetchRecipes', async (params: SearchParams) => {
  const { data } = await axios.post('/get-all-recipes', params);
  return data;
});

export const fetchRecipe = createAsyncThunk('recipes/fetchRecipe', async (id: number) => {
  const { data } = await axios.get(`/recipes/${id}`);
  return data;
});

export const fetchRemoveRecipe = createAsyncThunk('recipes/fetchRemoveRecipe', async (id: number) => {
  const { data } = await axios.delete(`/recipes/${id}`);
  return data;
});

export const fetchLike = createAsyncThunk('recipes/fetchLike', async (id: number) => {
  const { data } = await axios.post(`/recipe-like/${id}`);
  return data;
});

export const fetchUnlike = createAsyncThunk('recipes/fetchUnlike', async (id: number) => {
  const { data } = await axios.delete(`/recipe-like/${id}`);
  return data;
});
