import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from '../../axios/axios';

type LoginRequest = {
    email: string,
    password: string,
}

type RegisterRequest = {
    email: string,
    password: string,
    login: string,
}

export const fetchLogin = createAsyncThunk('/auth/fetchLogin', async (params: LoginRequest) => {
  const { data } = await axios.post('/auth/login', params);
  return data;
});

export const fetchRegister = createAsyncThunk('/auth/fetchRegister', async (params: RegisterRequest) => {
  const { data } = await axios.post('/auth/register', params);
  return data;
});

export const fetchMe = createAsyncThunk('/auth/me', async () => {
  const { data } = await axios.get('/me');
  return data;
});
