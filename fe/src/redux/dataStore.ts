import { ActionReducerMapBuilder, SerializedError } from '@reduxjs/toolkit';
import { RecipeModel } from '../Models/RecipeModel';
import { UserModel } from '../Models/UserModel';

export type DataStore<T> = {
    result?: T,
    loading: boolean,
    error?: SerializedError
}

export type UserStore = DataStore<UserModel>

export type RecipesStore = DataStore<{ items: RecipeModel[], totalCount: number }>

export type RecipeStore = DataStore<RecipeModel>

export const reducerSample = (builder: ActionReducerMapBuilder<DataStore<any>>, fetch: any) => {
  {
    builder.addCase(fetch.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetch.fulfilled, (state, { payload }) => {
      state.result = payload;
      state.loading = false;
    });
    builder.addCase(fetch.rejected, (state, { error }) => {
      state.result = undefined;
      state.loading = false;
      state.error = error;
    });
  }
};
