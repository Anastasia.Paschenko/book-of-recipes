import './App.css';
import { HomePage } from './Pages/HomePage/HomePage';
import { Header } from './Components/Header';
import Container from '@mui/material/Container';
import { RecipePage } from './Pages/RecipePage/RecipePage';
import { Routes, Route } from 'react-router-dom';
import { CreateRecipePage } from './Pages/CreateRecipePage/CreateRecipePage';
import { SignUpPage } from './Pages/SignUpPage/SignUpPage';
import { SignInPage } from './Pages/SignInPage/SignInPage';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { fetchMe } from './redux/fetchers/authFetchers';
import { UpdateRecipePage } from './Pages/UpdateRecipePage/UpdateRecipePage';

function App() {
  const dispatch = useDispatch<any>();

  useEffect(() => {
    dispatch(fetchMe());
  }, []);

  return (
    <>
      <Header />
      <div className='App'>
        <Container maxWidth="lg">
          <Routes>
            <Route path='/' element={<HomePage/>} />
            <Route path='/recipe/create' element={<CreateRecipePage/>} />
            <Route path='/recipe/:id' element={<RecipePage/>} />
            <Route path='/recipe/update/:id' element={<UpdateRecipePage/>} />
            <Route path='/signup' element={<SignUpPage/>} />
            <Route path='/signin' element={<SignInPage/>} />
          </Routes>
        </Container>
      </div>
    </>
  );
}

export default App;
