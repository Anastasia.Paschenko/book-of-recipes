export const seedRecipes = [
    {
        title: 'Cherry cupcakes',
        complexity: 'MEDIUM',
        method: '<p>Cook</p>',
        description: 'Cherry cupcakes',
        ingredients: "[{ \"name\": \"Sugar\", \"alternatives\": [] }]",
        authorId: 1,
        views: 0,
        tags: ["#Cherry", "#Cake"]
    },
    {
        title: 'Blueberries cupcakes',
        complexity: 'HARD',
        method: '<p>Cook</p>',
        description: 'Blueberries cupcakes',
        ingredients: "[{ \"name\": \"Blueberries\", \"alternatives\": [\"Cherry\", \"Strawberry\"] }]",
        authorId: 2,
        views: 0,
        tags: ["#Blueberries", "#Cake"]
    },
]