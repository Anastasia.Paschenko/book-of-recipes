export const seedUsers = [
    {
        email: 'john@john.com',
        login: 'john',
        role: 'user',
        password: 'password',
    },
    {
        email: 'ann@ann.com',
        login: 'ann',
        role: 'user',
        password: 'password',
    },
]