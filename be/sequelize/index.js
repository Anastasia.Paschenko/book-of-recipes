import { Sequelize } from 'sequelize';
import { defineModels, createModels } from './models/index.js';
import dotenv from 'dotenv'

dotenv.config({ path: '.env.local' })
dotenv.config()

export const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_LOGIN, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'postgres',
})

defineModels(sequelize)

export const authenticateDatabase = async () => {
  console.log(`Checking database connection...`);
  try {
    await sequelize.authenticate();
    console.log('Database connection OK!');
  } catch (error) {
    console.log('Unable to connect to the database:');
    console.log(error.message);
    process.exit(1);
  }
}

export const createTables = async () => {
  await createModels(sequelize)
}