import defineUser from './user.js'
import defineRecipe from './recipe.js'
import defineRecipeLike from './recipeLike.js'

export const defineModels = (sequelize) => {
    defineUser(sequelize)
    defineRecipe(sequelize)
    defineRecipeLike(sequelize)

    const { User, Recipe, RecipeLike } = sequelize.models;

    User.hasMany(Recipe, { as: "recipe" });
    Recipe.belongsTo(User, { as: "author" });

    User.belongsToMany(Recipe, { through: RecipeLike, as: "likedRecipes" });
    Recipe.belongsToMany(User, { through: RecipeLike, as: "clickedLikeUsers" });
}

export const createModels = async (sequelize) => {
    try {
        await sequelize.sync(({ alter: true }))
    } catch (err) {
        console.log(err)
    }
}