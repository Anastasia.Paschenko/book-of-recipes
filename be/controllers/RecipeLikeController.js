import { sequelize } from '../sequelize/index.js'
import { errors } from '../constants/messages.js';
import { Op } from "sequelize";

export const add = async (req, res) => {
    try {
        const doc = {
            RecipeId: Number(req.params.recipeId),
            UserId: req.userId,
        };
        const recipeLike = await sequelize.models.RecipeLike.create(doc)

        res.json(recipeLike);
    } catch (e) {
        res.status(500).json({
            message: errors.addLikeFail,
        });
    }
};

export const remove = async (req, res) => {
    try {
        const amountOfDeletedItems = await sequelize.models.RecipeLike.destroy({
            where: {
                [Op.and]: [{ UserId: req.userId }, { RecipeId: Number(req.params.recipeId) }]
            }
        })

        res.json({ amountOfDeletedItems });
    } catch (e) {
        res.status(500).json({
            message: errors.removeLikeFail,
        });
    }
};
