import bcrypt from 'bcrypt';
import { sequelize } from '../sequelize/index.js'
import { errors } from '../constants/messages.js';
import { createToken } from '../utils/token.js';

export const register = async (req, res) => {
    try {
        const password = req.body.password;
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);

        const doc = {
            email: req.body.email,
            login: req.body.login,
            role: 'user',
            passwordHash: hash,
        };

        const { dataValues: user } = await sequelize.models.User.create(doc)

        const { passwordHash, ...userData } = user;

        res.json({
            ...userData,
            token: createToken(user.id)
        });
    } catch (err) {
        res.status(500).json({
            message: errors.registartionFail,
        });
    }
};

export const login = async (req, res) => {
    try {
        const email = req.body.email;
        const data = await sequelize.models.User.findAll({ where: { email: email } });
        const user = data.length ? data[0].dataValues : null

        if (!user) {
            return res.status(400).json({
                message: errors.invalidLoginOrPassword,
            });
        }

        const isValidPass = await bcrypt.compare(req.body.password, user.passwordHash);

        if (!isValidPass) {
            return res.status(400).json({
                message: errors.invalidLoginOrPassword,
            });
        }

        const { passwordHash, ...userData } = user;

        res.json({
            ...userData,
            token: createToken(user.id),
        });
    } catch (err) {
        res.status(500).json({
            message: errors.loginFail,
        });
    }
};

export const getMe = async (req, res) => {
    try {
        const { dataValues: user } = await sequelize.models.User.findByPk(req.userId);

        if (!user) {
            return res.status(404).json({
                message: errors.userNotFound,
            });
        }

        const { passwordHash, ...userData } = user;
        res.json(userData);
    } catch (err) {
        res.status(500).json({
            message: errors.smthWentWrong,
        });
    }
};

