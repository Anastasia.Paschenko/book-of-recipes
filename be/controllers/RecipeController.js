import { sequelize } from '../sequelize/index.js'
import { errors } from '../constants/messages.js';
import { Op } from 'sequelize'

const include = ["author", "clickedLikeUsers"]

export const getAll = async (req, res) => {
    try {
        const count = await sequelize.models.Recipe.count()
        const limit = req.body.limit || 10
        const offset = req.body.page ? (req.body.page - 1) * limit : 0

        const filters = getFilters(req)

        let recipes = await sequelize.models.Recipe.findAll({
            attributes: {
                include: [
                    [sequelize.literal('(SELECT COUNT(*) FROM "RecipeLikes" WHERE "RecipeLikes"."RecipeId" = "Recipe"."id")'), '"likes"'],
                ]
            },
            include,
            offset,
            limit,
            order: [[sequelize.literal(`"${req.body.orderBy.field}"`), req.body.orderBy.type]],
            where: filters
        })

        recipes = recipes.map(recipe => {
            const { dataValues: { clickedLikeUsers, ...fields } } = recipe
            return {
                ...fields,
                likes: clickedLikeUsers.length,
                isLiked: req.userId ? clickedLikeUsers.some(({ dataValues: user }) => user.id == req.userId) : false
            }
        })

        res.json({
            totalCount: count,
            items: recipes
        });
    } catch (err) {
        res.status(500).json({
            message: errors.getAllRecipesFail,
        });
    }
};

export const getById = async (req, res) => {
    try {
        const recipeId = req.params.id;
        let recipe = await sequelize.models.Recipe.findByPk(recipeId, { include })
        recipe = await recipe.increment('views')

        const { dataValues: { clickedLikeUsers, ...fields } } = recipe
        recipe = {
            ...fields,
            likes: clickedLikeUsers.length,
            isLiked: req.userId ? clickedLikeUsers.some(({ dataValues: user }) => user.id == req.userId) : false
        }
        res.json(recipe);
    } catch (err) {
        res.status(500).json({
            message: errors.getRecipeFail,
        });
    }
};

export const getUserRecipes = async (req, res) => {
    try {
        const count = await sequelize.models.Recipe.count()
        const offset = req.query.offset || 0
        const limit = req.query.limit || 10
        const recipes = await sequelize.models.Recipe.findAll({ where: { AuthorId: req.userId }, include })
        res.json({
            totalCount: count,
            page: Math.floor(offset / limit + 1),
            items: recipes
        });
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: errors.getAllRecipesFail,
        });
    }
};

export const create = async (req, res) => {
    try {
        const doc = {
            title: req.body.title,
            method: req.body.method,
            ingredients: req.body.ingredients,
            tags: req.body.tags,
            authorId: req.userId,
            description: req.body.description,
            complexity: req.body.complexity,
            image: req.body.image,
            views: 0,
        };

        const user = await sequelize.models.User.findByPk(req.userId)
        if (!user) throw Error

        const recipe = await user.createRecipe(doc);
        res.json(recipe);
    } catch (e) {
        console.log(e)
        res.status(500).json({
            message: errors.createRecipeFail,
        });
    }
};

export const update = async (req, res) => {
    try {
        const doc = {
            title: req.body.title,
            method: req.body.method,
            ingredients: req.body.ingredients,
            tags: req.body.tags,
            description: req.body.description,
            complexity: req.body.complexity,
            image: req.body.image,
        };

        const user = await sequelize.models.User.findByPk(req.userId)
        if (!user) throw Error

        await sequelize.models.Recipe.update(
            doc,
            { where: { id: req.params.id } }
        )
        res.json({ id: req.params.id });
    } catch (e) {
        res.status(500).json({
            message: errors.updateRecipeFail,
        });
    }
};

export const remove = async (req, res) => {
    try {
        const recipeId = req.params.id;
        let recipe = await sequelize.models.Recipe.findByPk(recipeId)
        let amountOfDeletedItems = 0

        if (recipe.authorId === req.userId) {
            amountOfDeletedItems = await sequelize.models.Recipe.destroy({
                where: {
                    id: recipeId
                }
            })
        }

        res.json({ amountOfDeletedItems, id: recipeId });
    } catch (err) {
        res.status(500).json({
            message: errors.getRecipeFail,
        });
    }
};

const getFilters = (req) => {
    const search = req.body.filters?.searchString
    const complexity = req.body.filters?.complexity

    const filters = {}
    if (search || complexity) {
        filters[Op.and] = []

        if (search) {
            const searchFilter = {
                [Op.or]: [
                    { title: { [Op.substring]: search } },
                    { description: { [Op.substring]: search } },
                    { ingredients: { [Op.substring]: search } },
                ],
            }
            filters[Op.and].push(searchFilter)
        }

        if (complexity) {
            filters[Op.and].push({ complexity })
        }
    }

    return filters
}