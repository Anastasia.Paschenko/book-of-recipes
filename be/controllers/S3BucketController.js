import AWS from 'aws-sdk';
import fs from 'fs';
import dotenv from 'dotenv'
import { errors } from '../constants/messages.js';

dotenv.config({ path: '.env.local' })
dotenv.config()

const bucketName = process.env.AWS_S3_BUCKET_NAME
const accessKeyId = process.env.AWS_S3_ACCESS_KEY_ID
const secretAccessKey = process.env.AWS_S3_SECRET_ACCESS_KEY
const region = process.env.AWS_S3_REGION

const s3 = new AWS.S3({
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey,
    region
});


export const uploadImage = async (req, res) => {
    try {
        if (req.file == null) {
            return res.status(400).json({ message: errors.noUploadedFile })
        }
        let file = req.file
        let today = new Date().toISOString()
        const fileName = `${req.userId}/${today}-${file.originalname}`
        const uploadImage = async (file) => {
            const fileStream = fs.createReadStream(file.path);

            const params = {
                Bucket: bucketName,
                Key: fileName,
                Body: fileStream,
                ACL:'public-read',
            };

            return await s3.upload(params).promise()
        }
        const url = await uploadImage(file);

        return res.json({ path: url.Location });
    } catch (err) {
        res.status(500).json({
            message: errors.uploadedFileFail,
        });
    }
};
