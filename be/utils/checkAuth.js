import { errors } from '../constants/messages.js';
import { sequelize } from '../sequelize/index.js';
import { getIdFromToken } from './token.js';

export const getUserId = async (req, res, next) => {
  try {
    const userId = getIdFromToken(req.headers.authorization);
    const user = await sequelize.models.User.findByPk(userId);

    if (user) {
      req.userId = userId;
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: errors.smthWentWrong,
    });
  }
}

export const checkAuth = async (req, res, next) => {
  if (req.userId) return next()
  
  return res.status(403).json({
    message: errors.noAccess,
  });
}