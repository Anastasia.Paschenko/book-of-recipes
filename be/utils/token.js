import jwt from 'jsonwebtoken';

const secret = 'secretKey12345'

export const createToken = (userId) => {
    return jwt.sign(
        {
            id: userId,
        },
        secret,
        {
            expiresIn: '7d',
        },
    );
}

export const getIdFromToken = (authorization) => {
    if (!authorization) return
    
    const token = authorization.replace(/Bearer\s?/, '');
    const decoded = jwt.verify(token, secret);
    
    return decoded.id;
}