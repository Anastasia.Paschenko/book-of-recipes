import { body } from 'express-validator';
import { errors } from '../constants/messages.js';

export const registerValidation = [
  body('email', errors.incorrectEmail).isEmail(),
  body('password', errors.incorrectPassword).isLength({ min: 8 }),
  body('fullName', errors.incorrectEmail).isLength({ min: 3 }),
];
