import { createTables } from './sequelize/index.js'

createTables().then(() => {
    console.log('All models were synchronized successfully.')
}).catch(err => console.log(err))