import express from 'express';
import { RecipeController, UserController, RecipeLikeController, S3BucketController } from './controllers/index.js';
import { authenticateDatabase } from './sequelize/index.js'
import { checkAuth, getUserId } from './utils/checkAuth.js';
import { registerValidation } from './utils/validations.js';
import cors from 'cors';
import dotenv from 'dotenv'
import bodyParser from "body-parser";
import multer from 'multer';

dotenv.config({ path: '.env.local' })
dotenv.config()

const app = express();
app.use(express.json());
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

authenticateDatabase()

app.post('/auth/login', UserController.login);
app.post('/auth/register', registerValidation, UserController.register);
app.get('/me', getUserId, checkAuth, UserController.getMe);

app.post('/get-all-recipes', getUserId, RecipeController.getAll);
app.get('/recipes/:id', getUserId, RecipeController.getById);
app.get('/my-recipes', getUserId, checkAuth, RecipeController.getUserRecipes);
app.post('/recipes', getUserId, checkAuth, RecipeController.create);
app.patch('/recipes/:id', getUserId, checkAuth, RecipeController.update);
app.delete('/recipes/:id', getUserId, checkAuth, RecipeController.remove);

app.post('/recipe-like/:recipeId', getUserId, checkAuth, RecipeLikeController.add);
app.delete('/recipe-like/:recipeId', getUserId, checkAuth, RecipeLikeController.remove);

const upload = multer({ dest: 'uploads/' })
app.post("/uploadFile", upload.single('image'), getUserId, S3BucketController.uploadImage)

app.listen(process.env.SERVER_PORT, (err) => {
    if (err) {
        console.log(err)
        return
    }
    console.log('Server OK')
})