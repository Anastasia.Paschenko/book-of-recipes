import { sequelize } from './sequelize/index.js';
import { seedUsers } from './sequelize/seeds/users.js';
import { seedRecipes } from './sequelize/seeds/recipes.js';
import bcrypt from 'bcrypt';

const createUserSeeds = async () => {
    let result = 1
    try {
        for (let user of seedUsers) {
            const password = user.password;
            const salt = await bcrypt.genSalt(10);
            const hash = await bcrypt.hash(password, salt);

            const doc = {
                email: user.email,
                login: user.login,
                role: 'user',
                passwordHash: hash,
            };
            await sequelize.models.User.create(doc)
        }
    } catch (err) {
        console.log(err)
        result = 0
    }
    return result
}


const createRecipeSeeds = async () => {
    let result = 1
    for (let recipe of seedRecipes) {
        try {
            await sequelize.models.Recipe.create(recipe)
        } catch (err) {
            console.log(err)
            result = 0
        }
    }

    return result
}


createUserSeeds().then((result) => {
    if (result) {
        console.log('Users have been created')
    }
})

createRecipeSeeds().then((result) => {
    if (result) {
        console.log('Recipes have been created')
    }
})